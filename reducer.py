def reducer():
	salesTotal = 0
	oldkey = None

	for line in sys.stdin:
		data = line.strip().split("\t")
		if len(data) != 2:
			continue

		thisKey, thisSale = data
		
		# We want to see if the key is set and has changed since the last rwo has been read
		if 	oldKey and oldKey != thiskey:
			print "{0}\t{1}".format(oldKey, salesTotal)

			salesTotal = 0

		oldkey = thisKey
		salesTotal += float(thisSale)	


		if oldKey != None:
			print "{0}\t{1}".format(oldKey, salesTotal)